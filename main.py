#!/usr/bin/env python3
import os, sys
import logging
import time
import traceback
import subprocess
import io

import discord
from discord.ext import commands

import sbtex

bot = commands.Bot(command_prefix="/")
TMPDIR=os.path.join(os.path.dirname(os.path.abspath(__file__)),"tmp")
SBTEX_REPO = "sbtex/" #symlink

logging.basicConfig(level=logging.ERROR)

sbtex_logger = logging.getLogger("sbtex")
sbtex_logger.setLevel(logging.WARNING)

typesetter = sbtex.SbTypeset(
	strip_punc = True,
	logger = sbtex_logger
)

with open("token", "r") as f:
	TOKEN = f.read().strip()


@bot.command()
async def pull(ctx):
	with ctx.channel.typing():
		proc = subprocess.run(["git","-C",SBTEX_REPO,"pull"], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
		output = proc.stdout.decode()
	
	if(output == "Already up to date.\n"):
		await ctx.message.add_reaction(u"\u2611")
	else:
		await ctx.send(f"```\n{output}\n```")


@bot.command()
async def s(ctx, *words):
	if(words):
		print(f"<{str(ctx.author):15}>\t{list(words)}")
		blob = io.BytesIO(b"")
		words = list(words)
		before = time.time()
		try:
			with ctx.channel.typing():
				blob.write(typesetter.blob(words))
		except sbtex.SbTypesetError as e:
			await ctx.send(f"**Typeset Error:** `{e}`")
			return
		except:
			err = "".join(traceback.format_exception(*sys.exc_info()))
			await ctx.send(f"**Exception:**\n```\n{err}\n```")
			return
		
		after = time.time()
		raw_time = after - before
		blob.seek(0)
		await ctx.send(
			content="Typeset in **{0:.3f}s**.".format(raw_time),
			file = discord.File(blob, filename="sbtex.png")
		)
	
		del blob
	else:
		em = discord.Embed(
			title="SbTeX - Usage",
			description="/s *__words...__*\n/sheet"
		)
		await ctx.send(embed=em)

@bot.command()
async def sheet(ctx):
	await ctx.send(
		file = discord.File("Stratish.png")
	)
	
bot.run(TOKEN)
